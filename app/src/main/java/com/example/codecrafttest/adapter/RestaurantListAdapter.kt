package com.example.codecrafttest.adapter

import android.annotation.SuppressLint
import android.location.Location
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.codecrafttest.sputils.SpLocation
import com.example.codecrafttest.model.RestResult
import kotlinx.android.synthetic.main.restaurant_item.view.*


class RestaurantListAdapter : RecyclerView.Adapter<RestaurantListAdapter.MyViewHolder>() {

    private var restList: List<RestResult>? = null
    private var listener: OnRestItemClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(com.example.codecrafttest.R.layout.restaurant_item, parent, false)
        return MyViewHolder(itemView)

    }

    override fun getItemCount(): Int {
        return restList?.size ?: 0
    }

    fun submitList(list: List<RestResult>) {
        this.restList = list
        notifyDataSetChanged()
    }

    fun clear() {
        restList = null
        notifyDataSetChanged()
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val item = restList?.get(position)

        Glide.with(holder.itemView.context)
            .load(item?.icon)
            .into(holder.itemView.iv_restaurant)

        holder.itemView.tv_rest_name.text = item?.name
        holder.itemView.tv_rest_address.text = item?.vicinity
        holder.itemView.tv_rest_distance.text =
            item?.geometry?.location?.let { calculateDistance(it.lat, it.lng) }.toString() + "m"


        holder.itemView.setOnClickListener {
            if (position != RecyclerView.NO_POSITION) {
                item?.reference?.let { listener?.onItemClick(reference = it) }
            }
        }
    }

    private fun calculateDistance(restLat: Double, restLng: Double): Int {

        Log.d("calculateDistance ", "Lat : ${SpLocation.lat}, Lng : ${SpLocation.lng}")

        val distance = FloatArray(2)

        Location.distanceBetween(
            SpLocation.lat.toDouble(), SpLocation.lng.toDouble(),
            restLat, restLng, distance
        )

        return distance[0].toInt()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    interface OnRestItemClickListener {
        fun onItemClick(reference: String)
    }

    fun setOnRestItemClickListener(listener: OnRestItemClickListener) {
        this.listener = listener
    }
}