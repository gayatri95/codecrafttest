package com.example.codecrafttest.viewmodel

import android.app.Application
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.codecrafttest.model.Geometry
import com.example.codecrafttest.model.Location
import com.example.codecrafttest.model.RestResult
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class RestaurantListViewModel(application: Application) : AndroidViewModel(application) {

    val restList: MutableLiveData<List<RestResult>>?
        get() = listRestaurant

    var listRestaurant = MutableLiveData<List<RestResult>>()

    fun getListOfRestaurant(): MutableLiveData<List<RestResult>>? {
        return restList
    }

    fun callApiToGetRestaurant(latlng: String) {
        GetRestaurantList().execute(latlng)
    }

    inner class GetRestaurantList : AsyncTask<String, Void, ArrayList<RestResult>>() {

        val API_KEY = "AIzaSyAMZlvyYsOA1KnEYgULZF_9-vSk0qdNDHQ"
        private val PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place"
        private val TYPE_SEARCH = "/nearbysearch"
        private val OUT_JSON = "/json?"
        private val LOG_TAG = GetRestaurantList::class.java.simpleName


        val restList: ArrayList<RestResult> = ArrayList()


        override fun doInBackground(vararg p0: String?): ArrayList<RestResult> {

            var conn: HttpURLConnection? = null
            var jsonResults: String? = null
            try {
                val sb = StringBuilder(PLACES_API_BASE)
                sb.append(TYPE_SEARCH)
                sb.append(OUT_JSON)
                sb.append("radius=200")
                sb.append(
                    "&location=" + p0
                            [0]
                )
                sb.append("&type=restaurant")
                sb.append("&key=$API_KEY")

                val url = URL(sb.toString())
                conn = url.openConnection() as HttpURLConnection

                Log.e("Requset : ", url.toString())


                val responseCode = conn.responseCode // To Check for 200
                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    val inputStream = BufferedReader(InputStreamReader(conn.inputStream))
                    val response = StringBuffer()
                    var line: String?
                    do {
                        line = inputStream.readLine()

                        if (line == null)

                            break

                        println(line)
                        response.append(line)

                    } while (true)

                    inputStream.close()

                    jsonResults = response.toString()

                    Log.e("JsonResult : : ", jsonResults.toString())

                    parseResponse(jsonResults)

                } else {
                    println("ERROR ${conn.responseCode}")
                }

            } catch (e: MalformedURLException) {
                Log.e("TAG", "Error processing Places API URL", e)
            } catch (e: IOException) {
                Log.e("TAG", "Error connecting to Places API", e)
            } finally {
                conn?.disconnect()
            }

            return restList
        }

        private fun parseResponse(jsonResults: String) {
            try {
                val jsonObj = JSONObject(jsonResults)
                val restJsonArray = jsonObj.getJSONArray("results")

                Log.e("result : : ", restJsonArray.toString())

                if (restJsonArray.length() > 0) {
                    for (i in 0 until restJsonArray.length()) {

                        var rating = 0
                        val name = restJsonArray.getJSONObject(i).getString("name")
                        val vicinity = restJsonArray.getJSONObject(i).getString("vicinity")


                        if (restJsonArray.getJSONObject(i).has("rating")) {
                            rating = restJsonArray.getJSONObject(i).getInt("rating")
                        }

                        val geometryObj = restJsonArray.getJSONObject(i).getJSONObject("geometry")

                        val locationObj = geometryObj.getJSONObject("location")

                        val lat = locationObj.getDouble("lat")
                        val lng = locationObj.getDouble("lng")

                        val location = Location(lat,lng)
                        val geometry = Geometry(location = location)

                        val icon = restJsonArray.getJSONObject(i).getString("icon")

                        val result = RestResult(geometry = geometry, icon = icon, rating = rating, name = name,vicinity = vicinity)

                        restList.add(result)
                    }
                }

                Log.e("TAG List :", restList.toString())

            } catch (e: JSONException) {
                Log.e("ioufut", "Error processing JSON results", e)
            }

        }

        override fun onPostExecute(result: ArrayList<RestResult>?) {
            Log.e(LOG_TAG, result.toString())

            listRestaurant.value = result as List<RestResult>

//            Log.e(LOG_TAG, " Mutable Live List ${mutableLiveList.hasActiveObservers()}")
//            Log.e(LOG_TAG, " Mutable Live List ${mutableLiveList.hasObservers()}")
        }

    }

}