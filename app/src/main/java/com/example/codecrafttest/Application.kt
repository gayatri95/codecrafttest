package com.example.codecrafttest

import android.app.Application
import com.example.codecrafttest.sputils.SpLocation

class Application: Application() {

    override fun onCreate() {
        super.onCreate()
        SpLocation.init(this)
    }
}