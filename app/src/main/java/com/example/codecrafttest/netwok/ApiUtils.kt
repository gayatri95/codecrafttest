package com.example.codecrafttest.netwok

import android.os.AsyncTask
import android.util.Log
import com.example.codecrafttest.model.RestResult
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import javax.net.ssl.HttpsURLConnection


open class ApiUtils {

    val API_KEY = "AIzaSyAMZlvyYsOA1KnEYgULZF_9-vSk0qdNDHQ"
    private val PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place"
    private val TYPE_SEARCH = "/nearbysearch"
    private val OUT_JSON = "/json?"

    companion object {
        fun getInstance(): ApiUtils {
            return ApiUtils()
        }
    }

    open class ApiCall : AsyncTask<String, Void, String>() {

        override fun doInBackground(vararg p0: String?): String {

            Log.e("TAG", "Api call started")
            ApiUtils.getInstance().callApi(p0[0]!!)

            return ""
        }

    }

    public fun callApi(lat: String) {
        var conn: HttpURLConnection? = null
        var jsonResults: String? = null
        try {
            val sb = StringBuilder(PLACES_API_BASE)
            sb.append(TYPE_SEARCH)
            sb.append(OUT_JSON)
            sb.append("radius=90")
            sb.append("&location=$lat")
            sb.append("&type=restaurant")
            sb.append("&key=$API_KEY")

            val url = URL(sb.toString())
            conn = url.openConnection() as HttpURLConnection

//            val inputStream = InputStreamReader(conn.inputStream)
//            var read: Int
//            val buff = CharArray(1024)
//
//            while ((read = inputStream.read(buff)) != -1) {
//                jsonResults.append(buff, 0, read)
//            }


            val responseCode = conn.responseCode // To Check for 200
            if (responseCode == HttpsURLConnection.HTTP_OK) {

                val inputStream = BufferedReader(InputStreamReader(conn.inputStream))
                val response = StringBuffer()

                var line: String?

                do {

                    line = inputStream.readLine()

                    if (line == null)

                        break

                    println(line)
                    response.append(line)

                } while (true)

                inputStream.close()

                jsonResults = response.toString()

                Log.e("JsonResult : : ", jsonResults.toString())

                parseResponse(jsonResults)

            } else {
                println("ERROR ${conn.responseCode}")
            }

        } catch (e: MalformedURLException) {
            Log.e("TAG", "Error processing Places API URL", e)
        } catch (e: IOException) {
            Log.e("TAG", "Error connecting to Places API", e)
        } finally {
            conn?.disconnect()
        }


    }

    private fun parseResponse(jsonResults: String) {
        try {
            val restList: ArrayList<RestResult> = ArrayList()
            // Create a JSON object hierarchy from the results
            val jsonObj = JSONObject(jsonResults)
            val restJsonArray = jsonObj.getJSONArray("results")

            Log.e("result : : ", restJsonArray.toString())

            if (restJsonArray.length() > 0) {
                for (i in 0 until restJsonArray.length()) {

                    val name = restJsonArray.getJSONObject(i).getString("name")
                    val rating = restJsonArray.getJSONObject(i).getInt("rating")
                    val icon = restJsonArray.getJSONObject(i).getString("icon")

                    val result = RestResult(icon = icon, rating = rating, name = name)

                    restList.add(result)
                }
            }

            Log.e("TAG List :", restList.toString())

        } catch (e: JSONException) {
            Log.e("ioufut", "Error processing JSON results", e)
        }

    }

}