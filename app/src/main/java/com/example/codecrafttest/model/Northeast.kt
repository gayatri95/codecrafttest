package com.example.codecrafttest.model

data class Northeast(
    val lat: Double,
    val lng: Double
)