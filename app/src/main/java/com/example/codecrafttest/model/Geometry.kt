package com.example.codecrafttest.model

import java.io.Serializable

data class Geometry(
    val location: Location,
    val viewport: Viewport? = null
): Serializable