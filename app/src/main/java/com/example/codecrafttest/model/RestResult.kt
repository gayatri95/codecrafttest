package com.example.codecrafttest.model

import java.io.Serializable

data class RestResult(
    val geometry: Geometry? = null,
    val icon: String ?= null,
    val id: String? =null,
    val name: String? = null,
    val opening_hours: OpeningHours? = null,
    val photos: List<Photo>? = null,
    val place_id: String? = null,
    val plus_code: PlusCode? = null,
    val price_level: Int? = 0,
    val rating: Int? = 0,
    val reference: String? = null,
    val scope: String? = null,
    val types: List<String>? = null,
    val user_ratings_total: Int? = 0,
    val vicinity: String? = null
): Serializable