package com.example.codecrafttest.model

import java.io.Serializable

data class Viewport(
    val northeast: Northeast,
    val southwest: Southwest
): Serializable