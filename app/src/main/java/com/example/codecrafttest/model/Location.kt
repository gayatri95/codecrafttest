package com.example.codecrafttest.model

import java.io.Serializable

data class Location(
    val lat: Double,
    val lng: Double
): Serializable