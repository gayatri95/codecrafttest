package com.example.codecrafttest.model

import java.io.Serializable

data class PlusCode(
    val compound_code: String,
    val global_code: String
): Serializable