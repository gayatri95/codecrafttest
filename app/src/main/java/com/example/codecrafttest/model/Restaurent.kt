package com.example.codecrafttest.model

data class Restaurent(
    val html_attributions: List<Any>? = null,
    val results: List<RestResult>,
    val status: String ?= null
)