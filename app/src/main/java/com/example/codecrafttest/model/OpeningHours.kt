package com.example.codecrafttest.model

import java.io.Serializable

data class OpeningHours(
    val open_now: Boolean
): Serializable