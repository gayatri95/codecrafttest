package com.example.codecrafttest.model

data class Southwest(
    val lat: Double,
    val lng: Double
)