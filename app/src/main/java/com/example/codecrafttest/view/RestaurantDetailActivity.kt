package com.example.codecrafttest.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProviders
import com.example.codecrafttest.R
import com.example.codecrafttest.viewmodel.RestaurantDetailViewModel

class RestaurantDetailActivity : AppCompatActivity() {

    var restaurantDetailViewModel: RestaurantDetailViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant_detail)

        val reference = intent.getStringExtra("reference")

        Log.e("HeroDetailActivity : ", reference)


        restaurantDetailViewModel = ViewModelProviders.of(this).get(RestaurantDetailViewModel::class.java)




    }
}
