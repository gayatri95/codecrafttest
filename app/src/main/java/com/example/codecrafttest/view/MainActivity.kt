package com.example.codecrafttest.view

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.codecrafttest.R
import com.example.codecrafttest.sputils.SpLocation
import com.example.codecrafttest.adapter.RestaurantListAdapter
import com.example.codecrafttest.model.RestResult
import com.example.codecrafttest.viewmodel.RestaurantListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.io.Serializable


class MainActivity : AppCompatActivity(), ActivityCompat.OnRequestPermissionsResultCallback {

    var list: List<RestResult>? = ArrayList<RestResult>()
    var restaurantListViewModel: RestaurantListViewModel? = null
    val PERMISSION_REQUEST_LOCATION = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_list?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_list?.setHasFixedSize(true)

        val adapter = RestaurantListAdapter()
        rv_list?.adapter = adapter

        restaurantListViewModel = ViewModelProviders.of(this).get(RestaurantListViewModel::class.java)

        restaurantListViewModel?.getListOfRestaurant()?.observe(this,
            Observer<List<RestResult>> {
                list = it
                adapter.submitList(it)
                Log.e("Acitvity : ", it.toString())
            })


        pull_refresh?.setOnRefreshListener {
            //method call

            pull_refresh.isRefreshing = false
        }

        adapter.setOnRestItemClickListener(object : RestaurantListAdapter.OnRestItemClickListener {
            override fun onItemClick(reference: String) {

                val intent = Intent(this@MainActivity, RestaurantDetailActivity::class.java)
                intent.putExtra("reference", reference)
                startActivity(intent)
            }
        })
    }

    override fun onResume() {
        super.onResume()

        checkLocationPermission()

    }

    private fun checkLocationPermission() {

        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            getLatLng()

        } else {
            // Permission is missing and must be requested.
            ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSION_REQUEST_LOCATION
            )
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        if (requestCode == PERMISSION_REQUEST_LOCATION) {
            // Request for location permission.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLatLng()
            }
        }
    }

    private fun getLatLng() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
        ) {
            val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            val longitude = location!!.longitude
            val latitude = location.latitude
            val lng = java.lang.Double.toString(longitude)
            val lat = java.lang.Double.toString(latitude)

            Log.e("Lat ......", lat)
            Log.e("Lonit ......", lng)

            Log.d("TAG", "Lat and Long $lat,$lng")


            SpLocation.lat = latitude.toFloat()
            SpLocation.lng = longitude.toFloat()

            Log.d("SPLocation", "Lat : ${SpLocation.lat}, Lng : ${SpLocation.lng}")


            restaurantListViewModel?.callApiToGetRestaurant("$lat,$lng")

        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.map_view, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){

            R.id.map -> {

                val intent = Intent(this,MapsActivity::class.java)
                intent.putExtra("list",list as Serializable)
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)


    }

}
